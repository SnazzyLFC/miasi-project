﻿using System.Linq;
using Bank.Enums;
using Bank.Models.Commands;
using Bank.Models.Interests;
using Bank.Models.Managers;
using Bank.Models.Users;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bank.Tests
{
    [TestClass]
    public class BankChangeValueTests
    {
        private const string UserFirstName = "Testowicz";
        private const string UserLastName = "Testowy";
        private readonly Models.Bank.Bank _bank = new Models.Bank.Bank(new ProductManager(), "PKO");
        private readonly User _user = new User(UserFirstName, UserLastName);
        private const int Payment = 2000;

        [TestInitialize]
        public void Init()
        {
            _bank.CreateBankProduct(BankProductType.BankAccount, _user, typeof(InterestStateB));
        }

        [TestMethod]
        public void TestIncreaseAccountBalance()
        {
            var userProduct = _bank.GetUserProductsByType(_user.Id, BankProductType.BankAccount).First();
            userProduct?.PerformOperation(new AddValueCommand(userProduct, Payment));
            Assert.AreEqual(Payment, userProduct?.Balance);
        }
        
        [TestMethod]
        public void TestDecreaseAccountBalance()
        {
            const int withdrawal = 300;
            var userProduct = _bank.GetUserProductsByType(_user.Id, BankProductType.BankAccount).First();
            userProduct?.PerformOperation(new AddValueCommand(userProduct, Payment));
            userProduct?.PerformOperation(new RemoveValueCommand(userProduct, withdrawal));
            Assert.AreEqual(Payment - withdrawal, userProduct?.Balance);
        }
    }
}