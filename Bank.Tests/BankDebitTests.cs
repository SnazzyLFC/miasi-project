﻿using System.Linq;
using Bank.Enums;
using Bank.Models.BankProducts;
using Bank.Models.Commands;
using Bank.Models.Interests;
using Bank.Models.Managers;
using Bank.Models.Users;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bank.Tests
{
    [TestClass]
    public class BankDebitTests
    {
        private const string UserFirstName = "Testowicz";
        private const string UserLastName = "Testowy";
        private readonly Models.Bank.Bank _bank = new Models.Bank.Bank(new ProductManager(), "PKO");
        private readonly User _user = new User(UserFirstName, UserLastName);
        private const int Payment = 2000;
        private const int Debit = 200;
        private BankProductDebitDecorator userProduct;

        [TestInitialize]
        public void Init()
        {
            _bank.CreateBankProduct(BankProductType.BankProductDebitDecorator, _user,
                typeof(InterestStateB));
            userProduct = _bank.GetUserProductsByType(_user.Id, BankProductType.BankProductDebitDecorator).First() as
                BankProductDebitDecorator;
        }

        [TestMethod]
        public void TestAccountDebit()
        {
            userProduct?.PerformOperation(new AddValueCommand(userProduct, Payment));
            userProduct?.PerformOperation(new RemoveValueCommand(userProduct, Payment + Debit));
            Assert.AreEqual(Debit, userProduct?.CurrentDebit);
        }
        
        [TestMethod]
        public void TestAccountDebitInsufficientIncomeAccountStillEmpty()
        {
            userProduct?.PerformOperation(new AddValueCommand(userProduct, Payment));
            userProduct?.PerformOperation(new RemoveValueCommand(userProduct, Payment + Debit));
            userProduct?.PerformOperation(new AddValueCommand(userProduct, 50));
            
            Assert.AreEqual(0, userProduct?.Balance);
        }
        
        [TestMethod]
        public void TestAccountDebitInsufficientIncomeDebitDecreased()
        {
            userProduct?.PerformOperation(new AddValueCommand(userProduct, Payment));
            userProduct?.PerformOperation(new RemoveValueCommand(userProduct, Payment + Debit));
            userProduct?.PerformOperation(new AddValueCommand(userProduct, 50));
            
            Assert.AreEqual(Debit - 50, userProduct?.CurrentDebit);
        }
    }
}