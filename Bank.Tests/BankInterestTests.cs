﻿using System.Linq;
using Bank.Enums;
using Bank.Models.BankProducts;
using Bank.Models.Commands;
using Bank.Models.Interests;
using Bank.Models.Managers;
using Bank.Models.Users;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bank.Tests
{
    [TestClass]
    public class BankInterestTests
    {
        private const string UserFirstName = "Testowicz";
        private const string UserLastName = "Testowy";
        private readonly Models.Bank.Bank _bank = new Models.Bank.Bank(new ProductManager(), "PKO");
        private readonly User _user = new User(UserFirstName, UserLastName);
        private const int Payment = 2000;

        [TestInitialize]
        public void Init()
        {
            _bank.CreateBankProduct(BankProductType.BankAccount, _user, typeof(InterestStateC));
        }

        [TestMethod]
        public void TestAccountInterest()
        {
            var userProduct = _bank.GetUserProductsByType(_user.Id, BankProductType.BankAccount).First();
            userProduct?.PerformOperation(new AddValueCommand(userProduct, Payment));
            userProduct?.PerformOperation(new CalculateInterestCommand(userProduct));
            Assert.AreNotEqual(Payment, userProduct?.Balance);
        }

        [TestMethod]
        public void TestTwoAccountsSameValueDifferentInterest()
        {
            var anotherOwner = new User("Tomasz", "Jurtsch");
            _bank.CreateBankProduct(BankProductType.BankAccount, anotherOwner, typeof(InterestStateB));

            var firstUserProduct = _bank.GetUserProductsByType(_user.Id, BankProductType.BankAccount).First();
            firstUserProduct?.PerformOperation(new AddValueCommand(firstUserProduct, Payment));
            firstUserProduct?.PerformOperation(new CalculateInterestCommand(firstUserProduct));
            
            var secondUserProduct = _bank.GetUserProductsByType(anotherOwner.Id, BankProductType.BankAccount).First();
            secondUserProduct?.PerformOperation(new AddValueCommand(secondUserProduct, Payment));
            secondUserProduct?.PerformOperation(new CalculateInterestCommand(secondUserProduct));
            
            Assert.AreNotEqual(secondUserProduct?.Balance, firstUserProduct?.Balance);
        }
        
        [TestMethod]
        public void TestTwoAccountsSameValueAddInterestTwiceChangeInterestTypeInSecond()
        {
            var anotherOwner = new User("Tomasz", "Jurtsch");
            _bank.CreateBankProduct(BankProductType.BankAccount, anotherOwner, typeof(InterestStateC));

            var firstUserProduct = _bank.GetUserProductsByType(_user.Id, BankProductType.BankAccount).First();
            firstUserProduct?.PerformOperation(new AddValueCommand(firstUserProduct, Payment));
            firstUserProduct?.PerformOperation(new CalculateInterestCommand(firstUserProduct));
            firstUserProduct?.PerformOperation(new CalculateInterestCommand(firstUserProduct));
            
            var secondUserProduct = _bank.GetUserProductsByType(anotherOwner.Id, BankProductType.BankAccount).First();
            secondUserProduct?.PerformOperation(new AddValueCommand(secondUserProduct, Payment));
            secondUserProduct?.PerformOperation(new CalculateInterestCommand(secondUserProduct));
            secondUserProduct?.PerformOperation(new ChangeInterestTypeCommand(secondUserProduct, new InterestStateA()));
            secondUserProduct?.PerformOperation(new CalculateInterestCommand(secondUserProduct));
            
            Assert.AreNotEqual(secondUserProduct?.Balance, firstUserProduct?.Balance);
        }
    }
}