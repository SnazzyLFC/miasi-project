﻿using System.Linq;
using Bank.Enums;
using Bank.Models.Interests;
using Bank.Models.Managers;
using Bank.Models.Users;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bank.Tests
{
    [TestClass]
    public class BankTest
    {
        private const string UserFirstName = "Testowicz";
        private const string UserLastName = "Testowy";
        private readonly Models.Bank.Bank _bank = new Models.Bank.Bank(new ProductManager(), "PKO");
        private readonly User _user = new User(UserFirstName, UserLastName);
        
        [TestInitialize]
        public void Init()
        {
            _bank.CreateBankProduct(BankProductType.Deposit, _user, typeof(InterestStateB));
        }

        [TestMethod]
        public void TestCreatingBankProductWithCorrectBankProductType()
        {
            Assert.IsTrue(_bank.CreateBankProduct(BankProductType.Deposit, _user, typeof(InterestStateA)));
        }

        [TestMethod]
        public void TestNumberOfBankProductCreated()
        {
            Assert.AreEqual(1, _bank.GetProducts().Count);
        }

        [TestMethod]
        public void TestProductOwnerFirstName()
        {
            Assert.AreEqual(UserFirstName, _bank.GetProducts().First().Owner.FirstName);
        }

        [TestMethod]
        public void TestProductOwnerLastName()
        {
            Assert.AreEqual(UserLastName, _bank.GetProducts().First().Owner.LastName);
        }

        [TestMethod]
        public void TestProductCountByUserId()
        {
            _bank.CreateBankProduct(BankProductType.Loan, _user, typeof(InterestStateC));
            Assert.AreEqual(2, _bank.GetUserProducts(_user.Id).Count);
        }
    }
}