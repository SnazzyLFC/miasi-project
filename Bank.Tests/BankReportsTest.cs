﻿using System.Collections.Generic;
using System.Linq;
using Bank.Enums;
using Bank.Interfaces;
using Bank.Models.BankProducts;
using Bank.Models.Commands;
using Bank.Models.Interests;
using Bank.Models.Managers;
using Bank.Models.Reports;
using Bank.Models.Users;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bank.Tests
{
    [TestClass]
    public class BankReportsTest
    {
        private const string UserFirstName = "Testowicz";
        private const string UserLastName = "Testowy";
        private readonly Models.Bank.Bank _bank = new Models.Bank.Bank(new ProductManager(), "PKO");
        private readonly User _user = new User(UserFirstName, UserLastName);
        private readonly BalanceValueReportVisitor _balanceReport = new BalanceValueReportVisitor();
        private InterestTypeReportVisitor _interestReport;

        [TestInitialize]
        public void Init()
        {
            _bank.CreateBankProduct(BankProductType.BankAccount, _user, typeof(InterestStateB));
        }
        
        [TestMethod]
        public void TestBalanceReportAboveLimitSuccess()
        {
            var product = _bank.GetUserProductsByType(_user.Id, BankProductType.BankAccount).First() as BankAccount;
            const int sourceBalance = 1500;
            new AddValueCommand(product, sourceBalance).Execute();
            product?.Accept(_balanceReport);
            Assert.AreEqual(1, _balanceReport.ProductsSatisfyingContition.Count);

        }
        
        [TestMethod]
        public void TestBalanceReportBelowLimitSuccess()
        {
            var product = _bank.GetUserProductsByType(_user.Id, BankProductType.BankAccount).First() as BankAccount;
            const int sourceBalance = 800;
            new AddValueCommand(product, sourceBalance).Execute();
            product?.Accept(_balanceReport);
            Assert.AreEqual(0, _balanceReport.ProductsSatisfyingContition.Count);

        }
        
        [TestMethod]
        public void TestManyBalanceReportAboveLimitSuccess()
        {
            _bank.CreateBankProduct(BankProductType.BankAccount, _user, typeof(InterestStateB));
            _bank.CreateBankProduct(BankProductType.Loan, _user, typeof(InterestStateB));
            _bank.CreateBankProduct(BankProductType.Deposit, _user, typeof(InterestStateB));
            _bank.CreateBankProduct(BankProductType.BankAccount, _user, typeof(InterestStateB));
            
            var products = _bank.GetUserProducts(_user.Id);

            const int sourceBalance = 1500;
            products.ForEach(product =>
            {
                new AddValueCommand(product, sourceBalance).Execute();
                product.Accept(_balanceReport);
            });
            
            Assert.AreEqual(products.Count, _balanceReport.ProductsSatisfyingContition.Count);

        }
        
        [TestMethod]
        public void TestManyInterestTypeSuccess()
        {
            _bank.CreateBankProduct(BankProductType.BankAccount, _user, typeof(InterestStateB));
            _bank.CreateBankProduct(BankProductType.Loan, _user, typeof(InterestStateB));
            _bank.CreateBankProduct(BankProductType.Deposit, _user, typeof(InterestStateB));
            _bank.CreateBankProduct(BankProductType.BankAccount, _user, typeof(InterestStateB));
                        
            var products = _bank.GetUserProducts(_user.Id);
            
            _interestReport = new InterestTypeReportVisitor(typeof(InterestStateB));

            const int sourceBalance = 1500;
            products.ForEach(product =>
            {
                new AddValueCommand(product, sourceBalance).Execute();
                product.Accept(_interestReport);
            });
            
            Assert.AreEqual(products.Count, _interestReport.ProductsSatisfyingContition.Count);

        }
        
        [TestMethod]
        public void TestManyDifferentInterestTypeSuccess()
        {
            _bank.CreateBankProduct(BankProductType.BankAccount, _user, typeof(InterestStateB));
            _bank.CreateBankProduct(BankProductType.Loan, _user, typeof(InterestStateC));
            _bank.CreateBankProduct(BankProductType.Deposit, _user, typeof(InterestStateA));
                        
            var products = _bank.GetUserProducts(_user.Id);
            
            _interestReport = new InterestTypeReportVisitor(typeof(InterestStateA));

            const int sourceBalance = 1500;
            products.ForEach(product =>
            {
                new AddValueCommand(product, sourceBalance).Execute();
                product.Accept(_interestReport);
            });
            
            Assert.AreNotEqual(products.Count, _interestReport.ProductsSatisfyingContition.Count);

        }
    }
}