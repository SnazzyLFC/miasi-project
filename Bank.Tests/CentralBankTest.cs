﻿using System.Linq;
using Bank.Enums;
using Bank.Models.Bank;
using Bank.Models.BankProducts;
using Bank.Models.Commands;
using Bank.Models.Interests;
using Bank.Models.Managers;
using Bank.Models.Users;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bank.Tests
{
    [TestClass]
    public class CentralBankTest
    {
        private const string UserFirstName = "Testowicz";
        private const string UserLastName = "Testowy";
        private readonly Models.Bank.Bank _bank = new Models.Bank.Bank(new ProductManager(), "PKO");
        private readonly Models.Bank.Bank _targetBank = new Models.Bank.Bank(new ProductManager(), "ING");
        private readonly User _user = new User(UserFirstName, UserLastName);
        private readonly CentralBank _centralBank = new CentralBank();
        private const int SourceBalance = 2000;
        private const int TargetBalance = 1000;
        private const decimal TransferValue = 200;

        [TestInitialize]
        public void Init()
        {
            _bank.CreateBankProduct(BankProductType.BankAccount, _user, typeof(InterestStateB));
            _targetBank.CreateBankProduct(BankProductType.BankAccount, _user, typeof(InterestStateB));

            _centralBank.RegisterBank(_bank);
            _centralBank.RegisterBank(_targetBank);
        }

        [TestMethod]
        public void TestInterbankTransferSuccess()
        {
            var sourceProduct =
                _bank.GetUserProductsByType(_user.Id, BankProductType.BankAccount).First() as BankAccount;
            sourceProduct?.PerformOperation(new AddValueCommand(sourceProduct, SourceBalance));

            var targetProduct =
                _targetBank.GetUserProductsByType(_user.Id, BankProductType.BankAccount).First() as BankAccount;
            targetProduct?.PerformOperation(new AddValueCommand(targetProduct, TargetBalance));

            new InterbankTransferCommand(_targetBank.Id, targetProduct?.Id, _centralBank,
                sourceProduct, TransferValue, _centralBank.BankDictionary).Execute();

            Assert.AreEqual(SourceBalance - TransferValue, sourceProduct?.Balance);
        }

        [TestMethod]
        public void TestInterbankTransferWrongTargetIdAccountNotChanged()
        {
            var sourceProduct =
                _bank.GetUserProductsByType(_user.Id, BankProductType.BankAccount).First() as BankAccount;
            sourceProduct?.PerformOperation(new AddValueCommand(sourceProduct, SourceBalance));

            var targetProduct =
                _targetBank.GetUserProductsByType(_user.Id, BankProductType.BankAccount).First() as BankAccount;
            targetProduct?.PerformOperation(new AddValueCommand(targetProduct, TargetBalance));

            new InterbankTransferCommand(_targetBank.Id, "99", _centralBank,
                sourceProduct, TransferValue, _centralBank.BankDictionary).Execute();

            Assert.AreEqual(SourceBalance, sourceProduct?.Balance);
        }
    }
}