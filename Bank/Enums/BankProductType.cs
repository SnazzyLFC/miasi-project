﻿namespace Bank.Enums
{
    public enum BankProductType
    {
        BankAccount,
        Deposit,
        Loan,
        BankProductDebitDecorator
    }
}
