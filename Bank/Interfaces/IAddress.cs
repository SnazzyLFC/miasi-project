﻿namespace Bank.Interfaces
{
    public interface IAddress
    {
        void ChangeAddress(string streetName, int buildingNumber, string city, string postCode);
    }
}