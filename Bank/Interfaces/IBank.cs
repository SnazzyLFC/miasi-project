﻿using System;
using Bank.Enums;
using Bank.Models.Users;

namespace Bank.Interfaces
{
    public interface IBank
    {
        IUser CreateUser();
        void RemoveUser(string userId);

        bool CreateBankProduct(BankProductType productType, User owner, Type interestType);
        IBankProduct GetProductById(string id);

    }
}