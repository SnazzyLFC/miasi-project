﻿using Bank.Enums;

namespace Bank.Interfaces
{
    public interface IBankOperations
    {
        BankProductType Type { get; set; }    
        double AddValue(double value);
        double RemoveValue(double value);
        void AddInterest();
        void MakeTransfer(double value, IBankProduct destinationProduct);
        string GetOperationId();
    }
}
