﻿using System;
using System.Collections.Generic;
using Bank.Enums;
using Bank.Models.History;
using Bank.Models.Users;

namespace Bank.Interfaces
{
    public interface IBankProduct
    {
        BankProductType Type { get; }
        GlobalHistory History { get; }
        decimal Balance { get; }
        User Owner { get; }
        DateTime CreationDate { get; set; }
        void AddToBalance(decimal value);
        void RemoveFromBalance(decimal value);
        void PerformOperation(ICommand operation);
        IInterestState InterestState { set; get; }
        void Accept(IVisitor visitor);
        void CalculateInterest();
        void ChangeInterestType(IInterestState interestState);
    }
}