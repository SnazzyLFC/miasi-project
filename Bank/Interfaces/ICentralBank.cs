﻿using Bank.Models.Commands;

namespace Bank.Interfaces
{
    public interface ICentralBank
    {
        void Transfer(ICommand command);
        void TransferPayback(IBankProduct product, decimal value);
    }
}