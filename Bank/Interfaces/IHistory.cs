﻿using System.Collections.Generic;

namespace Bank.Interfaces
{
    public interface IHistory
    {
        void AddToHistory(IBankOperations operations);
        List<IBankOperations> GetHistory();
        IBankOperations GetSingleOperation(string operationId);
    }
}