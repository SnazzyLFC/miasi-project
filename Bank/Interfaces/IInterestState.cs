﻿namespace Bank.Interfaces
{
    public interface IInterestState
    {
        void CalculateInterest(IBankProduct product);
        decimal InterestValue { get; }
    }
}