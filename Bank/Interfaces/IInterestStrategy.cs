﻿namespace Bank.Interfaces
{
    public interface IInterestStrategy
    {
        decimal calculateInterest(IBankProduct product, decimal percent);
    }
}