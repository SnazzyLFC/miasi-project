﻿namespace Bank.Interfaces
{
    public interface IUser
    {
        string FirstName { get; }
        string LastName { get; }
    }
}