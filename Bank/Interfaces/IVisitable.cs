﻿namespace Bank.Interfaces
{
    public interface IVisitable
    {
        void Accept(IVisitor visitor);
    }
}