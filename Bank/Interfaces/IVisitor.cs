﻿using Bank.Models.BankProducts;

namespace Bank.Interfaces
{
    public interface IVisitor
    {
        void Visit(BankAccount account);
        void Visit(Deposit debitAccount);
        void Visit(Loan loan);
        void Visit(BankProductDebitDecorator decorator);
    }
}