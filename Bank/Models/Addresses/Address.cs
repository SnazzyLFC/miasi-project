﻿using System.ComponentModel.DataAnnotations;
using Bank.Interfaces;
using Bank.Models.Core;

namespace Bank.Models.Addresses
{
    public class Address : BaseModel, IAddress
    {
        [Required(AllowEmptyStrings = false)]
        private string StreetName { get; set; }

        private int BuildingNumber { get; set; }
        
        [Required(AllowEmptyStrings = false)]
        private string City { get; set; }
        
        [Required(AllowEmptyStrings = false)]
        [RegularExpression(@"[0-9]{2}\-[0-9]{3}")]
        private string PostCode { get; set; }

        public Address(string streetName, int buildingNumber, string city, string postCode)
        {
            StreetName = streetName;
            BuildingNumber = buildingNumber;
            City = city;
            PostCode = postCode;
        }

        public void ChangeAddress(string streetName, int buildingNumber, string city, string postCode)
        {
            this.StreetName = streetName;
            this.BuildingNumber = buildingNumber;
            this.City = city;
            this.PostCode = postCode;
        } 
    }
}
