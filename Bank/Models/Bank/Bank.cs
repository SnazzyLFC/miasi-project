﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bank.Enums;
using Bank.Interfaces;
using Bank.Models.Addresses;
using Bank.Models.Core;
using Bank.Models.History;
using Bank.Models.Managers;
using Bank.Models.Users;

namespace Bank.Models.Bank
{
    public class Bank : BaseModel, IBank
    {
        public string Name { get; }
        public Address Address { get; set; }
        public GlobalHistory History { get; set; }
        public ICollection<User> Users { get; }
        private readonly ProductManager _productManager;

        public Bank(ProductManager productManager, string name)
        {
            Users = new List<User>();
            _productManager = productManager;
            Name = name;
        }

        public IBankProduct GetProductById(string id)
        {
            return _productManager.GetProductById(id);
        }

        public ICollection<IBankProduct> GetProducts()
        {
            return _productManager.GetProducts();
        }

        public List<IBankProduct> GetUserProducts(string userId)
        {
            return _productManager.GetUserProducts(userId);
        }

        public List<IBankProduct> GetUserProductsByType(string userId, BankProductType type)
        {
            return _productManager.GetUserProductsByType(userId, type);
        }

        public GlobalHistory GetHistory()
        {
            return History;
        }

        public IUser CreateUser()
        {
            return new User();
        }

        public void RemoveUser(string userId)
        {
            try
            {
                Users.Remove(Users.FirstOrDefault(x => x.Id == userId));
            }
            catch (Exception)
            {
                throw new KeyNotFoundException();
            }
        }

        public bool CreateBankProduct(BankProductType productType, User owner, Type interestStrategyType)
        {
//            Users.Add(owner);
            return _productManager.CreateBankProduct(productType, owner, interestStrategyType);
        }
    }
}