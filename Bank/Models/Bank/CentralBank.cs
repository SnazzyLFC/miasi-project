﻿using System.Collections.Generic;
using Bank.Interfaces;
using Bank.Models.Commands;

namespace Bank.Models.Bank
{
    public class CentralBank : ICentralBank
    {
        public readonly Dictionary<string, IBank> BankDictionary = new Dictionary<string, IBank>();

        public void RegisterBank(Bank bank)
        {
            BankDictionary.Add(bank.Id, bank);
        }

        public void Transfer(ICommand command)
        {
            command.Execute();
        }

        public void TransferPayback(IBankProduct product, decimal value)
        {
            new RefusedTransferCommand(product, value).Execute();
        }
    }
}