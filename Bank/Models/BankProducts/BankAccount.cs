﻿using Bank.Enums;
using Bank.Interfaces;
using Bank.Models.Users;

namespace Bank.Models.BankProducts
{
    public class BankAccount : BankProduct, IVisitable
    {

        public BankAccount(User owner, BankProductType type) : base(owner, type)
        {

        }
        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
