﻿using System;
using Bank.Enums;
using Bank.Interfaces;
using Bank.Models.Core;
using Bank.Models.History;
using Bank.Models.Users;

namespace Bank.Models.BankProducts
{
    public abstract class BankProduct : BaseModel, IBankProduct
    {
        public BankProductType Type { get; set; }
        public GlobalHistory History { get; private set; }
        public decimal Balance { get; private set; }
        public User Owner { get; set; }
        public DateTime CreationDate { get; set; }
        public IInterestState InterestState { get; set; }

        protected BankProduct(User owner, BankProductType type)
        {
            Owner = owner;
            CreationDate = DateTime.UtcNow;
            Type = type;
            Balance = 0;
        }

        public virtual void AddToBalance(decimal value)
        {
            Balance += value;
        }

        public virtual void RemoveFromBalance(decimal value)
        {
            Balance -= value;
        }

        public void PerformOperation(ICommand command)
        {
            command.Execute();
        }

        public abstract void Accept(IVisitor visitor);
        
        public void CalculateInterest()
        {
            InterestState.CalculateInterest(this);
            AddToBalance(InterestState.InterestValue);
        }

        public void ChangeInterestType(IInterestState interestState)
        {
            InterestState = interestState;
        }
    }
}