﻿using System;
using Bank.Enums;
using Bank.Interfaces;
using Bank.Models.History;
using Bank.Models.Users;

namespace Bank.Models.BankProducts
{
    public class BankProductDebitDecorator : BankProduct, Interfaces.IBankProduct
    {
        private Interfaces.IBankProduct _realAccount;
        public decimal CurrentDebit { get; set; }
        private readonly int _maxDebit = 10000;

        public decimal Balance => _realAccount.Balance;

        public BankProductDebitDecorator(User owner, BankProductType type) : base(owner, type)
        {
            _realAccount = new BankAccount(owner, type);
            Type = type;
            Owner = owner;
        }

        public override void AddToBalance(decimal value)
        {
            if (CurrentDebit > value)
            {
                CurrentDebit -= value;
            }
            else
            {
                _realAccount.AddToBalance(value - CurrentDebit);
                CurrentDebit = 0;
            }
        }

        public override void RemoveFromBalance(decimal value)
        {
            if (_realAccount.Balance > value)
            {
                _realAccount.RemoveFromBalance(value);
            }
            else
            {
                CurrentDebit = value - _realAccount.Balance;
                _realAccount.RemoveFromBalance(_realAccount.Balance);
            }
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}