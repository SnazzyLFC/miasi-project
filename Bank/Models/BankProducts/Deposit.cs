﻿using Bank.Enums;
using Bank.Interfaces;
using Bank.Models.Users;

namespace Bank.Models.BankProducts
{
    public class Deposit : BankProduct, IVisitable
    {
        public Deposit(User owner, BankProductType type) : base(owner, type)
        {
            /// this.Balance = // withdraw from bank account
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}