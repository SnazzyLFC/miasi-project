﻿using Bank.Enums;
using Bank.Interfaces;
using Bank.Models.Users;

namespace Bank.Models.BankProducts
{
    public class Loan : BankProduct, IVisitable
    {
        public Loan(User owner, BankProductType type) : base(owner, type)
        {
            // deposit to  bankaccount
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
