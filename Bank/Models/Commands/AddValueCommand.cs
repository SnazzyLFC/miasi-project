﻿using Bank.Interfaces;

namespace Bank.Models.Commands
{
    public class AddValueCommand : ICommand
    {
        private readonly IBankProduct _account;
        private readonly decimal _value;

        public AddValueCommand(IBankProduct account, decimal value)
        {
            _account = account;
            _value = value;
        }
        
        public void Execute()
        {
            _account.AddToBalance(_value);
        }
    }
}