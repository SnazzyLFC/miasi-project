﻿using Bank.Interfaces;
 
 namespace Bank.Models.Commands
 {
     public class CalculateInterestCommand : ICommand
     {
         private readonly IBankProduct _product;
 
         public CalculateInterestCommand(IBankProduct product)
         {
             _product = product;
         }
 
         public void Execute()
         {
             _product.CalculateInterest();
         }
     }
 }