﻿using Bank.Interfaces;

namespace Bank.Models.Commands
{
    public class ChangeInterestTypeCommand : ICommand
    {
        
        private readonly IBankProduct _account;
        private readonly IInterestState _interestState;

        public ChangeInterestTypeCommand(IBankProduct account, IInterestState interestState)
        {
            _account = account;
            _interestState = interestState;
        }

        public void Execute()
        {
            _account.ChangeInterestType(_interestState);
        }
    }
}