﻿using System.Collections.Generic;
using Bank.Interfaces;
using Bank.Models.Bank;

namespace Bank.Models.Commands
{
    public class InterbankTransferCommand : ICommand
    {
        private readonly string _targetBankId;
        private readonly string _targetAccountId;
        private readonly ICentralBank _centralBank;
        private readonly IBankProduct _sourceProduct;
        private readonly decimal _value;
        private readonly Dictionary<string, IBank> _bankDictionary;
        

        public InterbankTransferCommand(string targetBankId, string targetAccountId, ICentralBank centralBank,
            IBankProduct sourceProduct, decimal value, Dictionary<string, IBank> bankDictionary)
        {
            _bankDictionary = bankDictionary;
            _targetBankId = targetBankId;
            _targetAccountId = targetAccountId;
            _centralBank = centralBank;
            _sourceProduct = sourceProduct;
            _value = value;
        }

        public void Execute()
        {
            new RemoveValueCommand(_sourceProduct, _value).Execute();

            _bankDictionary.TryGetValue(_targetBankId, out var targetBank);
            if (targetBank == null)
            {
                _centralBank.TransferPayback(_sourceProduct, _value);
                return;
            }

            var target = targetBank.GetProductById(_targetAccountId);

            if (target == null)
            {
                _centralBank.TransferPayback(_sourceProduct, _value);
                return;
            }

            new AddValueCommand(target, _value).Execute();
        }
    }
}