﻿using Bank.Interfaces;

namespace Bank.Models.Commands
{
    public class RefusedTransferCommand : ICommand
    {
        private IBankProduct TargetProduct { get; }
        private decimal Value { get; }

        public RefusedTransferCommand(IBankProduct targetProduct, decimal value)
        {
            TargetProduct = targetProduct;
            Value = value;
        }

        public void Execute()
        {
            new AddValueCommand(TargetProduct, Value).Execute();
        }
    }
}