﻿using Bank.Interfaces;

namespace Bank.Models.Commands
{
    public class RemoveValueCommand : ICommand
    {
        private readonly IBankProduct _account;
        private readonly decimal _value;

        public RemoveValueCommand(IBankProduct account, decimal value)
        {
            _value = value;
            _account = account;
        }
        
        public void Execute()
        {
            _account.RemoveFromBalance(_value);
        }
    }
}