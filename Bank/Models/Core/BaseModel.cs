﻿using System;

namespace Bank.Models.Core
{
    public abstract class BaseModel
    {
        public string Id { get; }

        protected BaseModel()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}