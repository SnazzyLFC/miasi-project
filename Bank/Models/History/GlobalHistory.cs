﻿using System.Collections.Generic;
using System.Linq;
using Bank.Interfaces;
using Bank.Models.Core;

namespace Bank.Models.History
{
    public class GlobalHistory : BaseModel, IHistory
    {
        private static GlobalHistory instance;
        private List<IBankOperations> globalOperationsHistory = new List<IBankOperations>();

        private GlobalHistory() { }
        public static GlobalHistory Instance
        {
            get { return instance ?? (instance = new GlobalHistory()); }
        }

        public void AddToHistory(IBankOperations operations)
        {
            globalOperationsHistory.Add(operations);
        }
        
        public List<IBankOperations> GetHistory()
        {
            return globalOperationsHistory;
        }

        public IBankOperations GetSingleOperation(string operationId)
        {
            return globalOperationsHistory.First(operation => operation.GetOperationId() == operationId);
        }
    }
}
