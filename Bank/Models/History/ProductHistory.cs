﻿using System.Collections.Generic;
using System.Linq;
using Bank.Interfaces;
using Bank.Models.Core;

namespace Bank.Models.History
{
    class ProductHistory : BaseModel, IHistory
    {
        private List<IBankOperations> productOperationsHistory = new List<IBankOperations>();

        public void AddToHistory(IBankOperations operations)
        {
            productOperationsHistory.Add(operations);
        }

        public List<IBankOperations> GetHistory()
        {
            return productOperationsHistory;
        }

        public IBankOperations GetSingleOperation(string operationId)
        {
            return productOperationsHistory.First(operation => operation.GetOperationId() == operationId);
        }
    }
}
