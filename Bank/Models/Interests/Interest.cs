﻿namespace Bank.Interfaces
{
    public class Interest
    {
        public IInterestStrategy Strategy { get; set; }
        public decimal Percent { get; set; }

        public Interest(IInterestStrategy strategy, decimal percent)
        {
            Strategy = strategy;
            Percent = percent;
        }
    }
}