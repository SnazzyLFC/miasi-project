﻿using Bank.Interfaces;
using Bank.Models.BankProducts;

namespace Bank.Models.Interests
{
    public class InterestStateA : IInterestState
    {
        public void CalculateInterest(IBankProduct product)
        {
            if (product.Balance <= 800)
            {
                InterestValue = product.Balance * 0.02m;
            }
            else
            {
                InterestValue = product.Balance * 0.04m;
            }
        }

        public decimal InterestValue { get; set; }
    }
}