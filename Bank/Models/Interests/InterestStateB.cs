﻿using Bank.Interfaces;
using Bank.Models.BankProducts;

namespace Bank.Models.Interests
{
    public class InterestStateB : IInterestState
    {
        public decimal InterestValue { get; set; }

        public void CalculateInterest(IBankProduct product)
        {
            if (product.Balance <= 1000)
            {
                InterestValue = product.Balance * 0.03m;
            }
            else
            {
                InterestValue = product.Balance * 0.05m;
            }
        }
    }
}