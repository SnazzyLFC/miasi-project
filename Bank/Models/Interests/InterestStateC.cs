﻿using Bank.Interfaces;
using Bank.Models.BankProducts;

namespace Bank.Models.Interests
{
    public class InterestStateC : IInterestState
    {
        public void CalculateInterest(IBankProduct product)
        {
            InterestValue = product.Balance * 0.035m;
        }

        public decimal InterestValue { get; set; }
    }
}