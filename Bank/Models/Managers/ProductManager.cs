﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bank.Enums;
using Bank.Interfaces;
using Bank.Models.BankProducts;
using Bank.Models.Interests;

namespace Bank.Models.Managers
{
    public class ProductManager
    {
        private readonly List<IBankProduct> _bankProducts;

        public ProductManager()
        {
            _bankProducts = new List<IBankProduct>();
        }

        public IBankProduct GetProductById(string id)
        {
            return _bankProducts.FirstOrDefault(product => (product as BankProduct)?.Id == id);
        }

        public ICollection<IBankProduct> GetProducts()
        {
            return _bankProducts;
        }

        public List<IBankProduct> GetUserProducts(string userId)
        {
            return _bankProducts.FindAll(product => product?.Owner.Id == userId);
        }

        public List<IBankProduct> GetUserProductsByType(string userId, BankProductType type)
        {
            _bankProducts.ForEach(product => Console.WriteLine(product.Owner.Id));


            return _bankProducts.FindAll(product =>
                product.Owner.Id == userId &&
                product.Type == type);
        }

        public bool CreateBankProduct(BankProductType productType, IUser owner, Type interestStrategyType)
        {
            try
            {
                var type = Type.GetType($"Bank.Models.BankProducts.{productType.ToString()}", true);
                if (!(Activator.CreateInstance(type, owner, productType) is IBankProduct product)) return true;
                product.InterestState = Activator.CreateInstance(interestStrategyType) as IInterestState;
                _bankProducts.Add(product);

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}