﻿using System.Collections.Generic;
using Bank.Interfaces;
using Bank.Models.BankProducts;
using Bank.Models.Core;
using IBankProduct = Bank.Interfaces.IBankProduct;

namespace Bank.Models.Reports
{
    public class BalanceValueReportVisitor : IVisitor
    {
        public List<IBankProduct> ProductsSatisfyingContition { get; } = new List<IBankProduct>();
        
        private void CheckProduct(IBankProduct product)
        {
            if (product.Balance > 1000)
            {
                ProductsSatisfyingContition.Add(product);
            } 
        }

        public void Visit(BankAccount account)
        {
            CheckProduct(account);
        }

        public void Visit(Deposit debitAccount)
        {
            CheckProduct(debitAccount);
        }

        public void Visit(Loan loan)
        {
            CheckProduct(loan);
        }

        public void Visit(BankProductDebitDecorator decorator)
        {
            CheckProduct(decorator);
        }
    }
}