﻿using System;
using System.Collections.Generic;
using Bank.Interfaces;
using Bank.Models.BankProducts;
using IBankProduct = Bank.Interfaces.IBankProduct;

namespace Bank.Models.Reports
{
    public class InterestTypeReportVisitor : IVisitor
    {
        public List<IBankProduct> ProductsSatisfyingContition { get; } = new List<IBankProduct>();
        private readonly Type _interestType;

        public InterestTypeReportVisitor(Type interestType)
        {
            _interestType = interestType;
        }

        private void CheckProduct(IBankProduct product)
        {
            if (product.InterestState.GetType() == _interestType)
            {
                ProductsSatisfyingContition.Add(product);
            } 
        }

        public void Visit(BankAccount account)
        {
            CheckProduct(account);
        }

        public void Visit(Deposit debitAccount)
        {
            CheckProduct(debitAccount);
        }

        public void Visit(Loan loan)
        {
            CheckProduct(loan);
        }

        public void Visit(BankProductDebitDecorator decorator)
        {
            CheckProduct(decorator);
        }
    }
}