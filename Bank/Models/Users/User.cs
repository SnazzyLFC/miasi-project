﻿using Bank.Interfaces;
using Bank.Models.Core;

namespace Bank.Models.Users
{
    public class User : BaseModel, IUser
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }

        public User(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public User()
        {
            FirstName = "John";
            LastName = "Smith";
        }
    }
}
